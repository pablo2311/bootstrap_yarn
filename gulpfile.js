var gulp = require('gulp'),
		sass = require ('gulp-sass'),
		notify = require('gulp-notify'),
		filter = require('gulp-filter'),
		autoprefixer = require('gulp-autoprefixer'),
		concat = require('gulp-concat'),
		uglify = require('gulp-uglify'),
		imagemin = require('gulp-imagemin'),
		browserSync = require('browser-sync').create();
		
var config = {
	stylesPath: 'assets/styles',
	jsPath: 'assets/scripts',
	outputDir: 'public/dist',
	imagesPath: 'assets/images'
}



gulp.task('icons', function() {
	return gulp.src('./node_modules/font-awesome/fonts/**.*')
		.pipe(gulp.dest(config.outputDir + '/fonts'));
});

gulp.task('images', function() {
	return gulp.src(config.imagesPath + '/*')
		.pipe(imagemin())
		.pipe(gulp.dest(config.outputDir + '/images'))
		.pipe(browserSync.stream());
});

gulp.task('css', function() {
	return gulp.src(config.stylesPath + '/main.scss')
		.pipe(sass({
				outputStyle: 'compressed',
				includePaths: [
					config.stylesPath,
					'./node_modules/bootstrap/scss',
					'./node_modules/font-awesome/scss'
				]
			}).on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(gulp.dest(config.outputDir + '/css'))
		.pipe(browserSync.stream());
});


gulp.task('jquery', function(){
	return gulp.src('./node_modules/jquery/dist/jquery.min.js')
		.pipe(gulp.dest(config.outputDir + '/js'));
});

gulp.task('bootstrap-js', function(){
	return gulp.src('./node_modules/bootstrap/dist/js/bootstrap.min.js')
		.pipe(gulp.dest(config.outputDir + '/js'));
});

gulp.task('js', function() {
	return gulp.src(config.jsPath+'/*')
		.pipe(filter('**/*.js'))
		.pipe(concat('main.js'))
		.pipe(uglify())
		.pipe(gulp.dest(config.outputDir + '/js'))
		.pipe(browserSync.stream());
});

gulp.task('watch', function browserDev(done) {
	browserSync.init({
    server: {
      baseDir: "./public/"
    }
  });
	gulp.watch([config.stylesPath + '**/*.scss', config.stylesPath + '**/*.sass', config.stylesPath + '**/*.css'], ['css']).on('change', browserSync.reload);
	gulp.watch([config.jsPath + '**/*.js'], ['js']).on('change', browserSync.reload);
	gulp.watch([config.imagesPath + '*'], ['images']).on('change', browserSync.reload);
	gulp.watch('./public/*.html').on('change', browserSync.reload);
});

gulp.task('default', ['icons', 'css', 'jquery', 'bootstrap-js', 'js', 'images']);